INSERT INTO public.m_roles ("name",description,last_update_by,last_update) VALUES
	 ('administrator','Administrator', 'system', CURRENT_DATE);


INSERT INTO public.m_users (user_name,email,"password",first_name,last_name,role_id,status,last_update_by,last_update) VALUES
	 ('admin','admin@google.com','240be518fabd2724ddb6f04eeb1da5967448d7e831c08c8fa822809f74c720a9','Admin','administrator',1,1,'system',CURRENT_DATE);


INSERT INTO public.m_menus ("name",description,link,parent_id,status,icon,"ordering") VALUES
	 ('master','Master','',0,1,'','100'),
	 ('order','Sales Order','',0,1,'','300'),
	 ('purchasing','Purchasing','',0,1,'','200'),
	 ('payment','Payment',NULL,0,1,NULL,'400'),
	 ('utility','Utility','',0,1,'','500'),
	 ('report','Report','',0,1,'','600');

INSERT INTO public.m_menus ("name",description,link,parent_id,status,icon,"ordering") VALUES
	 ('brand','Brand','brand',
	 (select id from m_menus m  where m."name" = 'master')
	 ,1,'','120'),
	 ('customer','Customer','customer',
	 (select id from m_menus m  where m."name" ='master')
	 ,1,'','125'),
	 ('product','Product','product',
	 (select id from m_menus m  where m."name" ='master')
	 ,1,'','135'),
	 ('product-group','Product Group','product-group',
	 (select id from m_menus m  where m."name" ='master')
	 ,1,'','130'),
	 ('sales','Salesman','salesman',
	 (select id from m_menus m  where m."name" ='master')
	 ,1,'','140'),
	 ('supplier','Supplier','supplier',
	 (select id from m_menus m  where m."name" ='master')
	 ,1,'','145'),
	 ('warehouse','Warehouse','warehouse',
	 (select id from m_menus m  where m."name" ='master')
	 ,1,'','150');
	
INSERT INTO public.m_menus ("name",description,link,parent_id,status,icon,"ordering") VALUES	
	 ('po','Create PO','purchase-order',
	  (select id from m_menus m  where m."name" = 'purchasing')
	  ,1,'','220'),
	 ('receiving','Receiving from Supplier','receive',
	 (select id from m_menus m  where m."name" = 'purchasing')
	 ,1,'','225'),
	 ('return-receiving','Return to Supplier','return-receive',
	(select id from m_menus m  where m."name" = 'purchasing')
	 ,1,'','230');

INSERT INTO public.m_menus ("name",description,link,parent_id,status,icon,"ordering") VALUES	 
	 ('direct-sales','Direct Sales','direct-sales',
	  (select id from m_menus m  where m."name" = 'order')
	 ,1,'','320'),
	 ('sales-order','Sales Order from Customer','sales-order',
	 (select id from m_menus m  where m."name" = 'order')
	 ,1,'','330'),
	 ('return-so','Return from Customer','sales-order-return',
	(select id from m_menus m  where m."name" = 'order')
	 ,1,'','340');
	
INSERT INTO public.m_menus ("name",description,link,parent_id,status,icon,"ordering") VALUES
	 ('mutation','Stock Mutation','stock-mutation',
	  (select id from m_menus m  where m."name" = 'utility')
	 ,1,'','520'),
	 ('adjusment','Adjustment','adjustment',
	   (select id from m_menus m  where m."name" = 'utility')
	 ,1,'','530'),
	 ('lookup','Lookup','lookup',
	  (select id from m_menus m  where m."name" = 'utility')
	 ,1,'','535'),
	 ('history','History Stock','history-stock',
	  (select id from m_menus m  where m."name" = 'utility')
	 ,1,'','540'),
	 ('role','Role','role',
	  (select id from m_menus m  where m."name" = 'utility')
	 ,1,'','545'),
	 ('user','Users','user',
	  (select id from m_menus m  where m."name" = 'utility')
	 ,1,'','550'),
	 ('stock','Stock Information','stock',
	  (select id from m_menus m  where m."name" = 'utility')
	 ,1,'','555'),
	 ('stock-opname','Stock Opname','stock-opname', 
	 (select id from m_menus m  where m."name" = 'utility')
	,1,'','525');
	
INSERT INTO public.m_menus ("name",description,link,parent_id,status,icon,"ordering") VALUES
	 ('report-payment','Report Payment','report-payment',
	 	 (select id from m_menus m  where m."name" = 'report')
	 ,1,'','620'),
	 ('report-sales','Report Penjualan','report-sales',
		 	 (select id from m_menus m  where m."name" = 'report')
	 ,1,'','630');	
	
INSERT INTO public.m_menus ("name",description,link,parent_id,status,icon,"ordering") VALUES
	 ('so-payment','Payment','payment',
	 (select id from m_menus m  where m."name" = 'payment')
	 ,1,'','420'),
	 ('direct-sales-payment','Direct Sales Payment','direct-sales-payment',
	(select id from m_menus m  where m."name" = 'payment')
	 ,1,'','560');
	 



INSERT INTO public.m_role_menu
(role_id, menu_id, status, last_update_by, last_update)
select (select id from m_roles mr limit 1 ) , id , 1, 'system', CURRENT_DATE  from m_menus;



INSERT INTO public.m_role_user 
(role_id,user_id,last_update_by,last_update,status)  
select 	(select id from m_roles limit 1), (select id from m_users limit 1) , 'system',CURRENT_DATE,1
;
	
