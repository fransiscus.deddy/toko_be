-- public.adjustment definition

-- Drop table

-- DROP TABLE public.adjustment;

CREATE TABLE public.adjustment (
	id bigserial NOT NULL,
	adjustment_no varchar(255) NULL,
	adjustment_date timestamptz NULL,
	note text NULL,
	total numeric NULL DEFAULT 0,
	status int2 NULL DEFAULT 0,
	last_update_by varchar(255) NULL,
	last_update timestamptz NULL,
	reason_id int8 NULL DEFAULT 0,
	warehouse_id int8 NULL,
	CONSTRAINT adjustment_pkey PRIMARY KEY (id)
);


-- public.adjustment_detail definition

-- Drop table

-- DROP TABLE public.adjustment_detail;

CREATE TABLE public.adjustment_detail (
	id bigserial NOT NULL,
	adjustment_id int8 NULL,
	product_id int8 NULL,
	qty numeric NULL,
	hpp numeric NULL DEFAULT 0,
	uom int8 NULL DEFAULT 0,
	last_update_by varchar(255) NULL,
	last_update timestamptz NULL,
	CONSTRAINT adjustment_detail_pkey PRIMARY KEY (id)
);


-- public.barcode definition

-- Drop table

-- DROP TABLE public.barcode;

CREATE TABLE public.barcode (
	id bigserial NOT NULL,
	product_id int8 NULL DEFAULT 0,
	barcode varchar(255) NULL,
	last_update_by varchar(255) NULL,
	last_update timestamptz NULL,
	CONSTRAINT barcode_pkey PRIMARY KEY (id)
);


-- public.brand definition

-- Drop table

-- DROP TABLE public.brand;

CREATE TABLE public.brand (
	id bigserial NOT NULL,
	"name" varchar(255) NULL,
	code varchar(255) NULL,
	status int2 NULL,
	last_update_by varchar(255) NULL,
	last_update timestamptz NULL,
	CONSTRAINT brand_pkey PRIMARY KEY (id)
);


-- public.customer definition

-- Drop table

-- DROP TABLE public.customer;

CREATE TABLE public.customer (
	id bigserial NOT NULL,
	"name" varchar(255) NULL,
	code varchar(255) NULL,
	top int4 NULL DEFAULT 0,
	address1 varchar(255) NULL,
	address2 varchar(255) NULL,
	address3 varchar(255) NULL,
	address4 varchar(255) NULL,
	contact_person varchar(255) NULL,
	phone_number varchar(255) NULL,
	status int2 NULL,
	last_update_by varchar(255) NULL,
	last_update timestamptz NULL,
	CONSTRAINT customer_pkey PRIMARY KEY (id)
);


-- public.history_stock definition

-- Drop table

-- DROP TABLE public.history_stock;

CREATE TABLE public.history_stock (
	id bigserial NOT NULL,
	"name" varchar(255) NULL,
	code varchar(255) NULL,
	debet numeric NULL DEFAULT 0,
	kredit numeric NULL DEFAULT 0,
	saldo numeric NULL DEFAULT 0,
	description varchar(255) NULL,
	trans_date timestamptz NULL,
	reff_no varchar(255) NULL,
	price numeric NULL DEFAULT 0,
	hpp numeric NULL DEFAULT 0,
	last_update_by varchar(255) NULL,
	last_update timestamptz NULL,
	disc1 numeric NULL,
	total numeric NULL,
	warehouse_id int8 NULL DEFAULT 0,
	CONSTRAINT history_stock_pkey PRIMARY KEY (id)
);


-- public.lookup definition

-- Drop table

-- DROP TABLE public.lookup;

CREATE TABLE public.lookup (
	id bigserial NOT NULL,
	"name" varchar(255) NULL,
	code varchar(255) NULL,
	lookup_group_id int8 NULL,
	status int2 NULL DEFAULT 1,
	is_viewable int2 NULL DEFAULT 1,
	CONSTRAINT lookup_pkey PRIMARY KEY (id)
);


-- public.lookup_group definition

-- Drop table

-- DROP TABLE public.lookup_group;

CREATE TABLE public.lookup_group (
	id bigserial NOT NULL,
	"name" varchar(255) NULL,
	CONSTRAINT lookup_group_pkey PRIMARY KEY (id)
);


-- public.m_menus definition

-- Drop table

-- DROP TABLE public.m_menus;

CREATE TABLE public.m_menus (
	id bigserial NOT NULL,
	"name" varchar(255) NULL,
	description varchar(255) NULL,
	link varchar(255) NULL,
	parent_id int8 NULL,
	status int2 NULL DEFAULT 1,
	icon varchar(255) NULL,
	"ordering" varchar NULL,
	CONSTRAINT m_menus_pkey PRIMARY KEY (id)
);


-- public.m_role_menu definition

-- Drop table

-- DROP TABLE public.m_role_menu;

CREATE TABLE public.m_role_menu (
	id bigserial NOT NULL,
	role_id int8 NOT NULL,
	menu_id int8 NOT NULL,
	status int2 NULL DEFAULT 1,
	last_update_by varchar(255) NULL,
	last_update timestamptz NULL,
	CONSTRAINT rolemenu_pkey PRIMARY KEY (id)
);


-- public.m_role_user definition

-- Drop table

-- DROP TABLE public.m_role_user;

CREATE TABLE public.m_role_user (
	role_id int8 NOT NULL,
	user_id int8 NOT NULL,
	last_update_by varchar(100) NULL,
	last_update timestamp NULL,
	status int4 NULL DEFAULT 0
);


-- public.m_roles definition

-- Drop table

-- DROP TABLE public.m_roles;

CREATE TABLE public.m_roles (
	id bigserial NOT NULL,
	"name" varchar(255) NULL,
	description varchar(255) NULL,
	last_update_by varchar(255) NULL,
	last_update timestamptz NULL,
	CONSTRAINT m_roles_pkey PRIMARY KEY (id)
);


-- public.m_users definition

-- Drop table

-- DROP TABLE public.m_users;

CREATE TABLE public.m_users (
	id bigserial NOT NULL,
	user_name varchar(255) NULL,
	email varchar(255) NULL,
	"password" varchar(255) NULL,
	first_name varchar(255) NULL,
	last_name varchar(255) NULL,
	role_id int8 NULL DEFAULT 0,
	status int2 NULL,
	last_update_by varchar(255) NULL,
	last_update timestamptz NULL,
	isadmin int4 NULL DEFAULT 1,
	CONSTRAINT m_users_pkey PRIMARY KEY (id)
);


-- public.mutation definition

-- Drop table

-- DROP TABLE public.mutation;

CREATE TABLE public.mutation (
	id bigserial NOT NULL,
	mutation_no varchar(255) NULL,
	mutation_date timestamptz NULL,
	warehouse_source int8 NULL DEFAULT 0,
	warehouse_dest int8 NULL DEFAULT 0,
	note text NULL,
	total numeric NULL DEFAULT 0,
	requestor varchar(255) NULL,
	approver varchar(255) NULL,
	status int2 NULL DEFAULT 1,
	last_update_by varchar(255) NULL,
	last_update timestamptz NULL,
	CONSTRAINT mutation_pkey PRIMARY KEY (id)
);


-- public.mutation_detail definition

-- Drop table

-- DROP TABLE public.mutation_detail;

CREATE TABLE public.mutation_detail (
	id bigserial NOT NULL,
	mutation_id int8 NULL DEFAULT 0,
	product_id int8 NULL DEFAULT 0,
	qty numeric NULL DEFAULT 0,
	uom numeric NULL DEFAULT 0,
	hpp numeric NULL DEFAULT 0,
	last_update_by varchar(255) NULL,
	last_update timestamptz NULL,
	CONSTRAINT mutation_detail_pkey PRIMARY KEY (id)
);


-- public.opname definition

-- Drop table

-- DROP TABLE public.opname;

CREATE TABLE public.opname (
	id bigserial NOT NULL,
	opname_no varchar(255) NULL,
	opname_date timestamptz NULL,
	opname_type_id int8 NULL,
	note text NULL,
	total numeric NULL DEFAULT 0,
	status int2 NULL DEFAULT 0,
	last_update_by varchar(255) NULL,
	last_update timestamptz NULL,
	CONSTRAINT opname_pkey PRIMARY KEY (id)
);


-- public.opname_detail definition

-- Drop table

-- DROP TABLE public.opname_detail;

CREATE TABLE public.opname_detail (
	id bigserial NOT NULL,
	opname_id int8 NULL,
	upload_file varchar(255) NULL,
	product_id int8 NULL,
	qty_system numeric NULL DEFAULT 0,
	qty_opname numeric NULL DEFAULT 0,
	hpp numeric NULL DEFAULT 0,
	total numeric NULL DEFAULT 0,
	uom int8 NULL DEFAULT 0,
	proccess int2 NULL DEFAULT 0,
	last_update_by varchar(255) NULL,
	last_update timestamptz NULL,
	CONSTRAINT opname_detail_pkey PRIMARY KEY (id)
);


-- public."parameter" definition

-- Drop table

-- DROP TABLE public."parameter";

CREATE TABLE public."parameter" (
	id bigserial NOT NULL,
	"name" varchar(255) NULL,
	value varchar(255) NULL,
	isviewable int2 NULL DEFAULT 1,
	CONSTRAINT parameter_pkey PRIMARY KEY (id)
);


-- public.payment definition

-- Drop table

-- DROP TABLE public.payment;

CREATE TABLE public.payment (
	id bigserial NOT NULL,
	payment_no varchar(255) NULL,
	payment_date timestamptz NULL,
	customer_id int8 NULL DEFAULT 0,
	note text NULL,
	total_order numeric NULL DEFAULT 0,
	total_payment numeric NULL DEFAULT 0,
	status int2 NULL DEFAULT 1,
	last_update_by varchar(255) NULL,
	last_update timestamptz NULL,
	total_return numeric NULL,
	is_cash bool NULL DEFAULT false,
	CONSTRAINT payment_pkey PRIMARY KEY (id)
);


-- public.payment_detail definition

-- Drop table

-- DROP TABLE public.payment_detail;

CREATE TABLE public.payment_detail (
	id bigserial NOT NULL,
	payment_id int8 NULL DEFAULT 0,
	payment_type_id int8 NULL DEFAULT 0,
	payment_reff varchar NULL DEFAULT 0,
	total numeric NULL DEFAULT 0,
	last_update_by varchar(255) NULL,
	last_update timestamptz NULL,
	CONSTRAINT payment_detail_pkey PRIMARY KEY (id)
);


-- public.payment_order definition

-- Drop table

-- DROP TABLE public.payment_order;

CREATE TABLE public.payment_order (
	id bigserial NOT NULL,
	payment_id int8 NULL DEFAULT 0,
	sales_order_id int8 NULL DEFAULT 0,
	total numeric NULL DEFAULT 0,
	last_update_by varchar(255) NULL,
	last_update timestamptz NULL,
	CONSTRAINT payment_order_pkey PRIMARY KEY (id)
);


-- public.payment_return definition

-- Drop table

-- DROP TABLE public.payment_return;

CREATE TABLE public.payment_return (
	id bigserial NOT NULL,
	payment_id int8 NULL DEFAULT 0,
	return_sales_order_id int8 NULL DEFAULT 0,
	total numeric NULL DEFAULT 0,
	last_update_by varchar(255) NULL,
	last_update timestamptz NULL,
	CONSTRAINT payment_order_return_pkey PRIMARY KEY (id)
);


-- public.payment_supplier definition

-- Drop table

-- DROP TABLE public.payment_supplier;

CREATE TABLE public.payment_supplier (
	id bigserial NOT NULL,
	payment_no varchar(255) NULL,
	payment_date timestamptz NULL,
	supplier_id int8 NULL DEFAULT 0,
	payment_type_id int8 NULL DEFAULT 0,
	payment_reff varchar NULL DEFAULT 0,
	note text NULL,
	total numeric NULL DEFAULT 0,
	status int2 NULL DEFAULT 1,
	last_update_by varchar(255) NULL,
	last_update timestamptz NULL,
	CONSTRAINT payment_supplier_pkey PRIMARY KEY (id)
);


-- public.payment_supplier_detail definition

-- Drop table

-- DROP TABLE public.payment_supplier_detail;

CREATE TABLE public.payment_supplier_detail (
	id bigserial NOT NULL,
	payment_supplier_id int8 NULL DEFAULT 0,
	receiving_id int8 NULL DEFAULT 0,
	total numeric NULL DEFAULT 0,
	last_update_by varchar(255) NULL,
	last_update timestamptz NULL,
	CONSTRAINT payment_supplier_detail_pkey PRIMARY KEY (id)
);


-- public.po definition

-- Drop table

-- DROP TABLE public.po;

CREATE TABLE public.po (
	id bigserial NOT NULL,
	po_no varchar(255) NULL,
	po_date timestamptz NULL,
	supplier_id int8 NULL DEFAULT 0,
	note text NULL,
	tax numeric NULL DEFAULT 0,
	total numeric NULL DEFAULT 0,
	grand_total numeric NULL DEFAULT 0,
	status int2 NULL DEFAULT 1,
	last_update_by varchar(255) NULL,
	last_update timestamptz NULL,
	sales_id int8 NULL DEFAULT 0,
	is_tax bool NULL DEFAULT false,
	CONSTRAINT po_pkey PRIMARY KEY (id)
);


-- public.po_detail definition

-- Drop table

-- DROP TABLE public.po_detail;

CREATE TABLE public.po_detail (
	id bigserial NOT NULL,
	po_id int8 NULL DEFAULT 0,
	product_id int8 NULL DEFAULT 0,
	qty numeric NULL DEFAULT 0,
	price numeric NULL DEFAULT 0,
	disc1 numeric NULL DEFAULT 0,
	disc2 numeric NULL DEFAULT 0,
	uom numeric NULL DEFAULT 0,
	last_update_by varchar(255) NULL,
	last_update timestamptz NULL,
	po_uom_id numeric NULL DEFAULT 0,
	po_uom_qty numeric NULL DEFAULT 1,
	po_qty numeric NULL DEFAULT 0,
	po_price numeric NULL DEFAULT 0,
	CONSTRAINT po_detail_pkey PRIMARY KEY (id)
);


-- public.product definition

-- Drop table

-- DROP TABLE public.product;

CREATE TABLE public.product (
	id bigserial NOT NULL,
	"name" varchar(255) NULL,
	code varchar(255) NULL,
	product_group_id int8 NULL,
	brand_id int8 NULL,
	big_uom_id int8 NULL,
	small_uom_id int8 NULL,
	status int2 NULL DEFAULT 1,
	qty_uom numeric NULL DEFAULT 1,
	sell_price numeric NULL DEFAULT 0,
	last_update_by varchar(255) NULL,
	last_update timestamptz NULL,
	hpp numeric NULL,
	sell_price_type int2 NULL DEFAULT 0,
	plu varchar(10) NULL,
	composition varchar(255) NULL,
	CONSTRAINT product_pkey PRIMARY KEY (id)
);


-- public.product_group definition

-- Drop table

-- DROP TABLE public.product_group;

CREATE TABLE public.product_group (
	id bigserial NOT NULL,
	"name" varchar(255) NULL,
	status int2 NULL DEFAULT 1,
	code varchar(255) NULL,
	last_update_by varchar(255) NULL,
	last_update timestamptz NULL,
	CONSTRAINT product_group_pkey PRIMARY KEY (id)
);


-- public.receive definition

-- Drop table

-- DROP TABLE public.receive;

CREATE TABLE public.receive (
	id bigserial NOT NULL,
	receive_no varchar(255) NULL,
	receive_date timestamptz NULL,
	supplier_id int8 NULL DEFAULT 0,
	note text NULL,
	tax numeric NULL DEFAULT 0,
	total numeric NULL DEFAULT 0,
	grand_total numeric NULL DEFAULT 0,
	status int2 NULL DEFAULT 1,
	last_update_by varchar(255) NULL,
	last_update timestamptz NULL,
	warehouse_id int8 NULL,
	invoice_no varchar(255) NULL,
	po_no varchar(255) NULL,
	is_paid bool NULL DEFAULT false,
	CONSTRAINT receive_pkey PRIMARY KEY (id)
);


-- public.receive_detail definition

-- Drop table

-- DROP TABLE public.receive_detail;

CREATE TABLE public.receive_detail (
	id bigserial NOT NULL,
	receive_id int8 NULL DEFAULT 0,
	product_id int8 NULL DEFAULT 0,
	qty numeric NULL DEFAULT 0,
	price numeric NULL DEFAULT 0,
	disc1 numeric NULL DEFAULT 0,
	uom numeric NULL DEFAULT 0,
	last_update_by varchar(255) NULL,
	last_update timestamptz NULL,
	disc2 numeric NULL DEFAULT 0,
	hpp numeric NULL,
	batch_no varchar(100) NULL DEFAULT ''::character varying,
	ed varchar(10) NULL DEFAULT ''::character varying,
	CONSTRAINT receive_detail_pkey PRIMARY KEY (id)
);


-- public.return_receive definition

-- Drop table

-- DROP TABLE public.return_receive;

CREATE TABLE public.return_receive (
	id bigserial NOT NULL,
	return_receive_no varchar(255) NULL,
	return_date timestamptz NULL,
	supplier_id int8 NULL DEFAULT 0,
	warehouse_id int8 NULL DEFAULT 0,
	reason_id int8 NULL DEFAULT 0,
	note text NULL,
	total numeric NULL DEFAULT 0,
	disc numeric NULL DEFAULT 0,
	tax numeric NULL DEFAULT 0,
	grand_total numeric NULL DEFAULT 0,
	status int2 NULL DEFAULT 1,
	last_update_by varchar(255) NULL,
	last_update timestamptz NULL,
	invoice_no varchar(255) NULL,
	is_paid bool NULL DEFAULT false,
	CONSTRAINT return_receive_pkey PRIMARY KEY (id)
);


-- public.return_receive_detail definition

-- Drop table

-- DROP TABLE public.return_receive_detail;

CREATE TABLE public.return_receive_detail (
	id bigserial NOT NULL,
	return_receive_id int8 NULL DEFAULT 0,
	product_id int8 NULL DEFAULT 0,
	qty numeric NULL DEFAULT 0,
	price numeric NULL DEFAULT 0,
	disc1 numeric NULL DEFAULT 0,
	disc2 numeric NULL DEFAULT 0,
	uom numeric NULL DEFAULT 0,
	last_update_by varchar(255) NULL,
	last_update timestamptz NULL,
	CONSTRAINT return_receive_detail_pkey PRIMARY KEY (id)
);


-- public.return_sales_order definition

-- Drop table

-- DROP TABLE public.return_sales_order;

CREATE TABLE public.return_sales_order (
	id bigserial NOT NULL,
	return_no varchar(255) NULL,
	return_date timestamptz NULL,
	customer_id int8 NULL DEFAULT 0,
	warehouse_id int8 NULL DEFAULT 0,
	reason_id int8 NULL DEFAULT 0,
	note text NULL,
	total numeric NULL DEFAULT 0,
	disc numeric NULL DEFAULT 0,
	tax numeric NULL DEFAULT 0,
	grand_total numeric NULL DEFAULT 0,
	status int2 NULL DEFAULT 1,
	last_update_by varchar(255) NULL,
	last_update timestamptz NULL,
	sales_id int8 NULL,
	invoice_no varchar(255) NULL,
	is_paid bool NULL,
	CONSTRAINT return_sales_order_pkey PRIMARY KEY (id)
);


-- public.return_sales_order_detail definition

-- Drop table

-- DROP TABLE public.return_sales_order_detail;

CREATE TABLE public.return_sales_order_detail (
	id bigserial NOT NULL,
	return_sales_order_id int8 NULL DEFAULT 0,
	product_id int8 NULL DEFAULT 0,
	qty numeric NULL DEFAULT 0,
	price numeric NULL DEFAULT 0,
	disc1 numeric NULL DEFAULT 0,
	disc2 numeric NULL DEFAULT 0,
	uom numeric NULL DEFAULT 0,
	last_update_by varchar(255) NULL,
	last_update timestamptz NULL,
	CONSTRAINT return_sales_order_detail_pkey PRIMARY KEY (id)
);


-- public.sales definition

-- Drop table

-- DROP TABLE public.sales;

CREATE TABLE public.sales (
	id bigserial NOT NULL,
	"name" varchar(255) NULL,
	description varchar(255) NULL,
	status int2 NULL,
	last_update_by varchar(255) NULL,
	last_update timestamptz NULL,
	code varchar(255) NULL,
	CONSTRAINT sales_pkey PRIMARY KEY (id)
);


-- public.sales_order definition

-- Drop table

-- DROP TABLE public.sales_order;

CREATE TABLE public.sales_order (
	id bigserial NOT NULL,
	sales_order_no varchar(255) NULL,
	order_date timestamptz NULL,
	customer_id int8 NULL DEFAULT 0,
	note text NULL,
	tax numeric NULL DEFAULT 0,
	total numeric NULL DEFAULT 0,
	grand_total numeric NULL DEFAULT 0,
	status int2 NULL DEFAULT 1,
	salesman_id int8 NULL DEFAULT 0,
	top int2 NULL DEFAULT 0,
	iscash int2 NULL DEFAULT 0,
	last_update_by varchar(255) NULL,
	last_update timestamptz NULL,
	warehouse_id int8 NULL DEFAULT 0,
	sales_id int8 NULL DEFAULT 0,
	picking_no varchar(255) NULL,
	picking_date timestamptz(0) NULL,
	picking_user varchar(255) NULL,
	delivery_no varchar(255) NULL,
	delivery_date timestamptz(0) NULL,
	delivery_driver varchar(255) NULL,
	invoice_no varchar(255) NULL,
	is_paid bool NULL,
	is_cash bool NULL DEFAULT false,
	CONSTRAINT sales_order_pkey PRIMARY KEY (id)
);


-- public.sales_order_detail definition

-- Drop table

-- DROP TABLE public.sales_order_detail;

CREATE TABLE public.sales_order_detail (
	id bigserial NOT NULL,
	sales_order_id int8 NULL DEFAULT 0,
	product_id int8 NULL DEFAULT 0,
	qty_order numeric NULL DEFAULT 0,
	price numeric NULL DEFAULT 0,
	disc1 numeric NULL DEFAULT 0,
	uom numeric NULL DEFAULT 0,
	last_update_by varchar(255) NULL,
	last_update timestamptz NULL,
	disc2 numeric NULL DEFAULT 0,
	qty_picking numeric NULL,
	qty_receive numeric NULL,
	hpp numeric NULL,
	CONSTRAINT sales_order_detail_pkey PRIMARY KEY (id)
);


-- public.stock definition

-- Drop table

-- DROP TABLE public.stock;

CREATE TABLE public.stock (
	id bigserial NOT NULL,
	product_id int8 NULL DEFAULT 0,
	status int2 NULL DEFAULT 1,
	warehouse_id int8 NULL DEFAULT 0,
	qty numeric NULL DEFAULT 0,
	last_update_by varchar(255) NULL,
	last_update timestamptz NULL,
	CONSTRAINT stock_pkey PRIMARY KEY (id)
);


-- public.stock_opname definition

-- Drop table

-- DROP TABLE public.stock_opname;

CREATE TABLE public.stock_opname (
	id bigserial NOT NULL,
	stock_opname_no varchar(255) NULL,
	stock_opname_date timestamptz NULL,
	note text NULL,
	total numeric NULL DEFAULT 0,
	status int2 NULL DEFAULT 0,
	last_update_by varchar(255) NULL,
	last_update timestamptz NULL,
	warehouse_id int8 NULL,
	CONSTRAINT stock_opname_pkey PRIMARY KEY (id)
);


-- public.stock_opname_detail definition

-- Drop table

-- DROP TABLE public.stock_opname_detail;

CREATE TABLE public.stock_opname_detail (
	id bigserial NOT NULL,
	stock_opname_id int8 NULL,
	product_id int8 NULL,
	qty numeric NULL,
	hpp numeric NULL DEFAULT 0,
	uom int8 NULL DEFAULT 0,
	last_update_by varchar(255) NULL,
	last_update timestamptz NULL,
	qty_on_system numeric NULL DEFAULT 0,
	CONSTRAINT stock_opname_detail_pkey PRIMARY KEY (id)
);


-- public.supplier definition

-- Drop table

-- DROP TABLE public.supplier;

CREATE TABLE public.supplier (
	id bigserial NOT NULL,
	"name" varchar(255) NULL,
	code varchar(255) NULL,
	address varchar(255) NULL,
	city varchar(255) NULL,
	pic_name varchar(255) NULL,
	pic_phone varchar(255) NULL,
	tax int4 NULL DEFAULT 0,
	bank_id int8 NULL DEFAULT 0,
	bank_acc_name varchar(255) NULL,
	bank_acc_no varchar(255) NULL,
	status int2 NULL,
	last_update_by varchar(255) NULL,
	last_update timestamptz NULL,
	CONSTRAINT supplier_pkey PRIMARY KEY (id)
);


-- public.tb_sequence definition

-- Drop table

-- DROP TABLE public.tb_sequence;

CREATE TABLE public.tb_sequence (
	id bigserial NOT NULL,
	subj varchar(255) NULL,
	"year" varchar(255) NULL,
	"month" varchar(255) NULL,
	last_seq int8 NOT NULL,
	last_update_by varchar(255) NULL,
	last_update timestamptz NULL
);


-- public.warehouse definition

-- Drop table

-- DROP TABLE public.warehouse;

CREATE TABLE public.warehouse (
	id bigserial NOT NULL,
	"name" varchar(255) NULL,
	wh_in int2 NULL DEFAULT 1,
	wh_out int2 NULL DEFAULT 1,
	status int2 NULL DEFAULT 1,
	code varchar(255) NULL,
	last_update_by varchar(255) NULL,
	last_update timestamptz NULL,
	CONSTRAINT warehouse_pkey PRIMARY KEY (id)
);


-- -- public.adjustment definition

-- -- Drop table

-- -- DROP TABLE public.adjustment;

-- CREATE TABLE public.adjustment (
-- 	id bigserial NOT NULL,
-- 	adjustment_no varchar(255) NULL,
-- 	adjustment_date timestamptz NULL,
-- 	note text NULL,
-- 	total numeric NULL DEFAULT 0,
-- 	status int2 NULL DEFAULT 0,
-- 	last_update_by varchar(255) NULL,
-- 	last_update timestamptz NULL,
-- 	reason_id int8 NULL DEFAULT 0,
-- 	warehouse_id int8 NULL,
-- 	CONSTRAINT adjustment_pkey PRIMARY KEY (id)
-- );


-- -- public.adjustment_detail definition

-- -- Drop table

-- -- DROP TABLE public.adjustment_detail;

-- CREATE TABLE public.adjustment_detail (
-- 	id bigserial NOT NULL,
-- 	adjustment_id int8 NULL,
-- 	product_id int8 NULL,
-- 	qty numeric NULL,
-- 	hpp numeric NULL DEFAULT 0,
-- 	uom int8 NULL DEFAULT 0,
-- 	last_update_by varchar(255) NULL,
-- 	last_update timestamptz NULL,
-- 	CONSTRAINT adjustment_detail_pkey PRIMARY KEY (id)
-- );


-- -- public.barcode definition

-- -- Drop table

-- -- DROP TABLE public.barcode;

-- CREATE TABLE public.barcode (
-- 	id bigserial NOT NULL,
-- 	product_id int8 NULL DEFAULT 0,
-- 	barcode varchar(255) NULL,
-- 	last_update_by varchar(255) NULL,
-- 	last_update timestamptz NULL,
-- 	CONSTRAINT barcode_pkey PRIMARY KEY (id)
-- );


-- -- public.brand definition

-- -- Drop table

-- -- DROP TABLE public.brand;

-- CREATE TABLE public.brand (
-- 	id bigserial NOT NULL,
-- 	"name" varchar(255) NULL,
-- 	code varchar(255) NULL,
-- 	status int2 NULL,
-- 	last_update_by varchar(255) NULL,
-- 	last_update timestamptz NULL,
-- 	CONSTRAINT brand_pkey PRIMARY KEY (id)
-- );


-- -- public.customer definition

-- -- Drop table

-- -- DROP TABLE public.customer;

-- CREATE TABLE public.customer (
-- 	id bigserial NOT NULL,
-- 	"name" varchar(255) NULL,
-- 	code varchar(255) NULL,
-- 	top int4 NULL DEFAULT 0,
-- 	address1 varchar(255) NULL,
-- 	address2 varchar(255) NULL,
-- 	address3 varchar(255) NULL,
-- 	address4 varchar(255) NULL,
-- 	contact_person varchar(255) NULL,
-- 	phone_number varchar(255) NULL,
-- 	status int2 NULL,
-- 	last_update_by varchar(255) NULL,
-- 	last_update timestamptz NULL,
-- 	CONSTRAINT customer_pkey PRIMARY KEY (id)
-- );


-- -- public.history_stock definition

-- -- Drop table

-- -- DROP TABLE public.history_stock;

-- CREATE TABLE public.history_stock (
-- 	id bigserial NOT NULL,
-- 	"name" varchar(255) NULL,
-- 	code varchar(255) NULL,
-- 	debet numeric NULL DEFAULT 0,
-- 	kredit numeric NULL DEFAULT 0,
-- 	saldo numeric NULL DEFAULT 0,
-- 	description varchar(255) NULL,
-- 	trans_date timestamptz NULL,
-- 	reff_no varchar(255) NULL,
-- 	price numeric NULL DEFAULT 0,
-- 	hpp numeric NULL DEFAULT 0,
-- 	last_update_by varchar(255) NULL,
-- 	last_update timestamptz NULL,
-- 	disc1 numeric NULL,
-- 	total numeric NULL,
-- 	warehouse_id int8 NULL DEFAULT 0,
-- 	CONSTRAINT history_stock_pkey PRIMARY KEY (id)
-- );


-- -- public.lookup definition

-- -- Drop table

-- -- DROP TABLE public.lookup;

-- CREATE TABLE public.lookup (
-- 	id bigserial NOT NULL,
-- 	"name" varchar(255) NULL,
-- 	code varchar(255) NULL,
-- 	lookup_group_id int8 NULL,
-- 	status int2 NULL DEFAULT 1,
-- 	is_viewable int2 NULL DEFAULT 1,
-- 	CONSTRAINT lookup_pkey PRIMARY KEY (id)
-- );


-- -- public.lookup_group definition

-- -- Drop table

-- -- DROP TABLE public.lookup_group;

-- CREATE TABLE public.lookup_group (
-- 	id bigserial NOT NULL,
-- 	"name" varchar(255) NULL,
-- 	CONSTRAINT lookup_group_pkey PRIMARY KEY (id)
-- );


-- -- public.m_menus definition

-- -- Drop table

-- -- DROP TABLE public.m_menus;

-- CREATE TABLE public.m_menus (
-- 	id bigserial NOT NULL,
-- 	"name" varchar(255) NULL,
-- 	description varchar(255) NULL,
-- 	link varchar(255) NULL,
-- 	parent_id int8 NULL,
-- 	status int2 NULL DEFAULT 1,
-- 	icon varchar(255) NULL,
-- 	"ordering" varchar NULL,
-- 	CONSTRAINT m_menus_pkey PRIMARY KEY (id)
-- );


-- -- public.m_role_menu definition

-- -- Drop table

-- -- DROP TABLE public.m_role_menu;

-- CREATE TABLE public.m_role_menu (
-- 	id bigserial NOT NULL,
-- 	role_id int8 NOT NULL,
-- 	menu_id int8 NOT NULL,
-- 	status int2 NULL DEFAULT 1,
-- 	last_update_by varchar(255) NULL,
-- 	last_update timestamptz NULL,
-- 	CONSTRAINT rolemenu_pkey PRIMARY KEY (id)
-- );


-- -- public.m_role_user definition

-- -- Drop table

-- -- DROP TABLE public.m_role_user;

-- CREATE TABLE public.m_role_user (
-- 	role_id int8 NOT NULL,
-- 	user_id int8 NOT NULL,
-- 	last_update_by varchar(100) NULL,
-- 	last_update timestamp NULL,
-- 	status int4 NULL DEFAULT 0
-- );


-- -- public.m_roles definition

-- -- Drop table

-- -- DROP TABLE public.m_roles;

-- CREATE TABLE public.m_roles (
-- 	id bigserial NOT NULL,
-- 	"name" varchar(255) NULL,
-- 	description varchar(255) NULL,
-- 	last_update_by varchar(255) NULL,
-- 	last_update timestamptz NULL,
-- 	CONSTRAINT m_roles_pkey PRIMARY KEY (id)
-- );


-- -- public.m_users definition

-- -- Drop table

-- -- DROP TABLE public.m_users;

-- CREATE TABLE public.m_users (
-- 	id bigserial NOT NULL,
-- 	user_name varchar(255) NULL,
-- 	email varchar(255) NULL,
-- 	"password" varchar(255) NULL,
-- 	first_name varchar(255) NULL,
-- 	last_name varchar(255) NULL,
-- 	role_id int8 NULL DEFAULT 0,
-- 	status int2 NULL,
-- 	last_update_by varchar(255) NULL,
-- 	last_update timestamptz NULL,
-- 	CONSTRAINT m_users_pkey PRIMARY KEY (id)
-- );


-- -- public.mutation definition

-- -- Drop table

-- -- DROP TABLE public.mutation;

-- CREATE TABLE public.mutation (
-- 	id bigserial NOT NULL,
-- 	mutation_no varchar(255) NULL,
-- 	mutation_date timestamptz NULL,
-- 	warehouse_source int8 NULL DEFAULT 0,
-- 	warehouse_dest int8 NULL DEFAULT 0,
-- 	note text NULL,
-- 	total numeric NULL DEFAULT 0,
-- 	requestor varchar(255) NULL,
-- 	approver varchar(255) NULL,
-- 	status int2 NULL DEFAULT 1,
-- 	last_update_by varchar(255) NULL,
-- 	last_update timestamptz NULL,
-- 	CONSTRAINT mutation_pkey PRIMARY KEY (id)
-- );


-- -- public.mutation_detail definition

-- -- Drop table

-- -- DROP TABLE public.mutation_detail;

-- CREATE TABLE public.mutation_detail (
-- 	id bigserial NOT NULL,
-- 	mutation_id int8 NULL DEFAULT 0,
-- 	product_id int8 NULL DEFAULT 0,
-- 	qty numeric NULL DEFAULT 0,
-- 	uom numeric NULL DEFAULT 0,
-- 	hpp numeric NULL DEFAULT 0,
-- 	last_update_by varchar(255) NULL,
-- 	last_update timestamptz NULL,
-- 	CONSTRAINT mutation_detail_pkey PRIMARY KEY (id)
-- );


-- -- public.opname definition

-- -- Drop table

-- -- DROP TABLE public.opname;

-- CREATE TABLE public.opname (
-- 	id bigserial NOT NULL,
-- 	opname_no varchar(255) NULL,
-- 	opname_date timestamptz NULL,
-- 	opname_type_id int8 NULL,
-- 	note text NULL,
-- 	total numeric NULL DEFAULT 0,
-- 	status int2 NULL DEFAULT 0,
-- 	last_update_by varchar(255) NULL,
-- 	last_update timestamptz NULL,
-- 	CONSTRAINT opname_pkey PRIMARY KEY (id)
-- );


-- -- public.opname_detail definition

-- -- Drop table

-- -- DROP TABLE public.opname_detail;

-- CREATE TABLE public.opname_detail (
-- 	id bigserial NOT NULL,
-- 	opname_id int8 NULL,
-- 	upload_file varchar(255) NULL,
-- 	product_id int8 NULL,
-- 	qty_system numeric NULL DEFAULT 0,
-- 	qty_opname numeric NULL DEFAULT 0,
-- 	hpp numeric NULL DEFAULT 0,
-- 	total numeric NULL DEFAULT 0,
-- 	uom int8 NULL DEFAULT 0,
-- 	proccess int2 NULL DEFAULT 0,
-- 	last_update_by varchar(255) NULL,
-- 	last_update timestamptz NULL,
-- 	CONSTRAINT opname_detail_pkey PRIMARY KEY (id)
-- );


-- -- public."parameter" definition

-- -- Drop table

-- -- DROP TABLE public."parameter";

-- CREATE TABLE public."parameter" (
-- 	id bigserial NOT NULL,
-- 	"name" varchar(255) NULL,
-- 	value varchar(255) NULL,
-- 	isviewable int2 NULL DEFAULT 1,
-- 	CONSTRAINT parameter_pkey PRIMARY KEY (id)
-- );


-- -- public.payment definition

-- -- Drop table

-- -- DROP TABLE public.payment;

-- CREATE TABLE public.payment (
-- 	id bigserial NOT NULL,
-- 	payment_no varchar(255) NULL,
-- 	payment_date timestamptz NULL,
-- 	customer_id int8 NULL DEFAULT 0,
-- 	note text NULL,
-- 	total_order numeric NULL DEFAULT 0,
-- 	total_payment numeric NULL DEFAULT 0,
-- 	status int2 NULL DEFAULT 1,
-- 	last_update_by varchar(255) NULL,
-- 	last_update timestamptz NULL,
-- 	total_return numeric NULL,
-- 	is_cash bool NULL DEFAULT false,
-- 	CONSTRAINT payment_pkey PRIMARY KEY (id)
-- );


-- -- public.payment_detail definition

-- -- Drop table

-- -- DROP TABLE public.payment_detail;

-- CREATE TABLE public.payment_detail (
-- 	id bigserial NOT NULL,
-- 	payment_id int8 NULL DEFAULT 0,
-- 	payment_type_id int8 NULL DEFAULT 0,
-- 	payment_reff varchar NULL DEFAULT 0,
-- 	total numeric NULL DEFAULT 0,
-- 	last_update_by varchar(255) NULL,
-- 	last_update timestamptz NULL,
-- 	CONSTRAINT payment_detail_pkey PRIMARY KEY (id)
-- );


-- -- public.payment_order definition

-- -- Drop table

-- -- DROP TABLE public.payment_order;

-- CREATE TABLE public.payment_order (
-- 	id bigserial NOT NULL,
-- 	payment_id int8 NULL DEFAULT 0,
-- 	sales_order_id int8 NULL DEFAULT 0,
-- 	total numeric NULL DEFAULT 0,
-- 	last_update_by varchar(255) NULL,
-- 	last_update timestamptz NULL,
-- 	CONSTRAINT payment_order_pkey PRIMARY KEY (id)
-- );


-- -- public.payment_return definition

-- -- Drop table

-- -- DROP TABLE public.payment_return;

-- CREATE TABLE public.payment_return (
-- 	id int8 NOT NULL,
-- 	payment_id int8 NULL DEFAULT 0,
-- 	return_sales_order_id int8 NULL DEFAULT 0,
-- 	total numeric NULL DEFAULT 0,
-- 	last_update_by varchar(255) NULL,
-- 	last_update timestamptz NULL,
-- 	CONSTRAINT payment_order_return_pkey PRIMARY KEY (id)
-- );


-- -- public.po definition

-- -- Drop table

-- -- DROP TABLE public.po;

-- CREATE TABLE public.po (
-- 	id bigserial NOT NULL,
-- 	po_no varchar(255) NULL,
-- 	po_date timestamptz NULL,
-- 	supplier_id int8 NULL DEFAULT 0,
-- 	note text NULL,
-- 	tax numeric NULL DEFAULT 0,
-- 	total numeric NULL DEFAULT 0,
-- 	grand_total numeric NULL DEFAULT 0,
-- 	status int2 NULL DEFAULT 1,
-- 	last_update_by varchar(255) NULL,
-- 	last_update timestamptz NULL,
-- 	sales_id int8 NULL DEFAULT 0,
-- 	is_tax bool NULL DEFAULT false,
-- 	CONSTRAINT po_pkey PRIMARY KEY (id)
-- );


-- -- public.po_detail definition

-- -- Drop table

-- -- DROP TABLE public.po_detail;

-- CREATE TABLE public.po_detail (
-- 	id bigserial NOT NULL,
-- 	po_id int8 NULL DEFAULT 0,
-- 	product_id int8 NULL DEFAULT 0,
-- 	qty numeric NULL DEFAULT 0,
-- 	price numeric NULL DEFAULT 0,
-- 	disc1 numeric NULL DEFAULT 0,
-- 	disc2 numeric NULL DEFAULT 0,
-- 	uom numeric NULL DEFAULT 0,
-- 	last_update_by varchar(255) NULL,
-- 	last_update timestamptz NULL,
-- 	CONSTRAINT po_detail_pkey PRIMARY KEY (id)
-- );


-- -- public.product definition

-- -- Drop table

-- -- DROP TABLE public.product;

-- CREATE TABLE public.product (
-- 	id bigserial NOT NULL,
-- 	"name" varchar(255) NULL,
-- 	code varchar(255) NULL,
-- 	product_group_id int8 NULL,
-- 	brand_id int8 NULL,
-- 	big_uom_id int8 NULL,
-- 	small_uom_id int8 NULL,
-- 	status int2 NULL DEFAULT 1,
-- 	qty_uom numeric NULL DEFAULT 1,
-- 	sell_price numeric NULL DEFAULT 0,
-- 	last_update_by varchar(255) NULL,
-- 	last_update timestamptz NULL,
-- 	hpp numeric NULL,
-- 	sell_price_type int2 NULL DEFAULT 0,
-- 	plu varchar(10) NULL,
-- 	composition varchar(255) NULL,
-- 	CONSTRAINT product_pkey PRIMARY KEY (id)
-- );


-- -- public.product_group definition

-- -- Drop table

-- -- DROP TABLE public.product_group;

-- CREATE TABLE public.product_group (
-- 	id bigserial NOT NULL,
-- 	"name" varchar(255) NULL,
-- 	status int2 NULL DEFAULT 1,
-- 	code varchar(255) NULL,
-- 	last_update_by varchar(255) NULL,
-- 	last_update timestamptz NULL,
-- 	CONSTRAINT product_group_pkey PRIMARY KEY (id)
-- );


-- -- public.receive definition

-- -- Drop table

-- -- DROP TABLE public.receive;

-- CREATE TABLE public.receive (
-- 	id bigserial NOT NULL,
-- 	receive_no varchar(255) NULL,
-- 	receive_date timestamptz NULL,
-- 	supplier_id int8 NULL DEFAULT 0,
-- 	note text NULL,
-- 	tax numeric NULL DEFAULT 0,
-- 	total numeric NULL DEFAULT 0,
-- 	grand_total numeric NULL DEFAULT 0,
-- 	status int2 NULL DEFAULT 1,
-- 	last_update_by varchar(255) NULL,
-- 	last_update timestamptz NULL,
-- 	warehouse_id int8 NULL,
-- 	invoice_no varchar(255) NULL,
-- 	po_no varchar(255) NULL,
-- 	CONSTRAINT receive_pkey PRIMARY KEY (id)
-- );


-- -- public.receive_detail definition

-- -- Drop table

-- -- DROP TABLE public.receive_detail;

-- CREATE TABLE public.receive_detail (
-- 	id bigserial NOT NULL,
-- 	receive_id int8 NULL DEFAULT 0,
-- 	product_id int8 NULL DEFAULT 0,
-- 	qty numeric NULL DEFAULT 0,
-- 	price numeric NULL DEFAULT 0,
-- 	disc1 numeric NULL DEFAULT 0,
-- 	uom numeric NULL DEFAULT 0,
-- 	last_update_by varchar(255) NULL,
-- 	last_update timestamptz NULL,
-- 	disc2 numeric NULL DEFAULT 0,
-- 	hpp numeric NULL,
-- 	CONSTRAINT receive_detail_pkey PRIMARY KEY (id)
-- );


-- -- public.return_receive definition

-- -- Drop table

-- -- DROP TABLE public.return_receive;

-- CREATE TABLE public.return_receive (
-- 	id bigserial NOT NULL,
-- 	return_receive_no varchar(255) NULL,
-- 	return_date timestamptz NULL,
-- 	supplier_id int8 NULL DEFAULT 0,
-- 	warehouse_id int8 NULL DEFAULT 0,
-- 	reason_id int8 NULL DEFAULT 0,
-- 	note text NULL,
-- 	total numeric NULL DEFAULT 0,
-- 	disc numeric NULL DEFAULT 0,
-- 	tax numeric NULL DEFAULT 0,
-- 	grand_total numeric NULL DEFAULT 0,
-- 	status int2 NULL DEFAULT 1,
-- 	last_update_by varchar(255) NULL,
-- 	last_update timestamptz NULL,
-- 	invoice_no varchar(255) NULL,
-- 	CONSTRAINT return_receive_pkey PRIMARY KEY (id)
-- );


-- -- public.return_receive_detail definition

-- -- Drop table

-- -- DROP TABLE public.return_receive_detail;

-- CREATE TABLE public.return_receive_detail (
-- 	id bigserial NOT NULL,
-- 	return_receive_id int8 NULL DEFAULT 0,
-- 	product_id int8 NULL DEFAULT 0,
-- 	qty numeric NULL DEFAULT 0,
-- 	price numeric NULL DEFAULT 0,
-- 	disc1 numeric NULL DEFAULT 0,
-- 	disc2 numeric NULL DEFAULT 0,
-- 	uom numeric NULL DEFAULT 0,
-- 	last_update_by varchar(255) NULL,
-- 	last_update timestamptz NULL,
-- 	CONSTRAINT return_receive_detail_pkey PRIMARY KEY (id)
-- );


-- -- public.return_sales_order definition

-- -- Drop table

-- -- DROP TABLE public.return_sales_order;

-- CREATE TABLE public.return_sales_order (
-- 	id bigserial NOT NULL,
-- 	return_no varchar(255) NULL,
-- 	return_date timestamptz NULL,
-- 	customer_id int8 NULL DEFAULT 0,
-- 	warehouse_id int8 NULL DEFAULT 0,
-- 	reason_id int8 NULL DEFAULT 0,
-- 	note text NULL,
-- 	total numeric NULL DEFAULT 0,
-- 	disc numeric NULL DEFAULT 0,
-- 	tax numeric NULL DEFAULT 0,
-- 	grand_total numeric NULL DEFAULT 0,
-- 	status int2 NULL DEFAULT 1,
-- 	last_update_by varchar(255) NULL,
-- 	last_update timestamptz NULL,
-- 	sales_id int8 NULL,
-- 	invoice_no varchar(255) NULL,
-- 	is_paid bool NULL,
-- 	CONSTRAINT return_sales_order_pkey PRIMARY KEY (id)
-- );


-- -- public.return_sales_order_detail definition

-- -- Drop table

-- -- DROP TABLE public.return_sales_order_detail;

-- CREATE TABLE public.return_sales_order_detail (
-- 	id bigserial NOT NULL,
-- 	return_sales_order_id int8 NULL DEFAULT 0,
-- 	product_id int8 NULL DEFAULT 0,
-- 	qty numeric NULL DEFAULT 0,
-- 	price numeric NULL DEFAULT 0,
-- 	disc1 numeric NULL DEFAULT 0,
-- 	disc2 numeric NULL DEFAULT 0,
-- 	uom numeric NULL DEFAULT 0,
-- 	last_update_by varchar(255) NULL,
-- 	last_update timestamptz NULL,
-- 	CONSTRAINT return_sales_order_detail_pkey PRIMARY KEY (id)
-- );


-- -- public.sales definition

-- -- Drop table

-- -- DROP TABLE public.sales;

-- CREATE TABLE public.sales (
-- 	id bigserial NOT NULL,
-- 	"name" varchar(255) NULL,
-- 	description varchar(255) NULL,
-- 	status int2 NULL,
-- 	last_update_by varchar(255) NULL,
-- 	last_update timestamptz NULL,
-- 	code varchar(255) NULL,
-- 	CONSTRAINT sales_pkey PRIMARY KEY (id)
-- );


-- -- public.sales_order definition

-- -- Drop table

-- -- DROP TABLE public.sales_order;

-- CREATE TABLE public.sales_order (
-- 	id bigserial NOT NULL,
-- 	sales_order_no varchar(255) NULL,
-- 	order_date timestamptz NULL,
-- 	customer_id int8 NULL DEFAULT 0,
-- 	note text NULL,
-- 	tax numeric NULL DEFAULT 0,
-- 	total numeric NULL DEFAULT 0,
-- 	grand_total numeric NULL DEFAULT 0,
-- 	status int2 NULL DEFAULT 1,
-- 	salesman_id int8 NULL DEFAULT 0,
-- 	top int2 NULL DEFAULT 0,
-- 	iscash int2 NULL DEFAULT 0,
-- 	last_update_by varchar(255) NULL,
-- 	last_update timestamptz NULL,
-- 	warehouse_id int8 NULL DEFAULT 0,
-- 	sales_id int8 NULL DEFAULT 0,
-- 	picking_no varchar(255) NULL,
-- 	picking_date timestamptz(0) NULL,
-- 	picking_user varchar(255) NULL,
-- 	delivery_no varchar(255) NULL,
-- 	delivery_date timestamptz(0) NULL,
-- 	delivery_driver varchar(255) NULL,
-- 	invoice_no varchar(255) NULL,
-- 	is_paid bool NULL,
-- 	is_cash bool NULL DEFAULT false,
-- 	CONSTRAINT sales_order_pkey PRIMARY KEY (id)
-- );


-- -- public.sales_order_detail definition

-- -- Drop table

-- -- DROP TABLE public.sales_order_detail;

-- CREATE TABLE public.sales_order_detail (
-- 	id bigserial NOT NULL,
-- 	sales_order_id int8 NULL DEFAULT 0,
-- 	product_id int8 NULL DEFAULT 0,
-- 	qty_order numeric NULL DEFAULT 0,
-- 	price numeric NULL DEFAULT 0,
-- 	disc1 numeric NULL DEFAULT 0,
-- 	uom numeric NULL DEFAULT 0,
-- 	last_update_by varchar(255) NULL,
-- 	last_update timestamptz NULL,
-- 	disc2 numeric NULL DEFAULT 0,
-- 	qty_picking numeric NULL,
-- 	qty_receive numeric NULL,
-- 	hpp numeric NULL,
-- 	CONSTRAINT sales_order_detail_pkey PRIMARY KEY (id)
-- );


-- -- public.stock definition

-- -- Drop table

-- -- DROP TABLE public.stock;

-- CREATE TABLE public.stock (
-- 	id bigserial NOT NULL,
-- 	product_id int8 NULL DEFAULT 0,
-- 	status int2 NULL DEFAULT 1,
-- 	warehouse_id int8 NULL DEFAULT 0,
-- 	qty numeric NULL DEFAULT 0,
-- 	last_update_by varchar(255) NULL,
-- 	last_update timestamptz NULL,
-- 	CONSTRAINT stock_pkey PRIMARY KEY (id)
-- );


-- -- public.stock_opname definition

-- -- Drop table

-- -- DROP TABLE public.stock_opname;

-- CREATE TABLE public.stock_opname (
-- 	id bigserial NOT NULL,
-- 	stock_opname_no varchar(255) NULL,
-- 	stock_opname_date timestamptz NULL,
-- 	note text NULL,
-- 	total numeric NULL DEFAULT 0,
-- 	status int2 NULL DEFAULT 0,
-- 	last_update_by varchar(255) NULL,
-- 	last_update timestamptz NULL,
-- 	warehouse_id int8 NULL,
-- 	CONSTRAINT stock_opname_pkey PRIMARY KEY (id)
-- );


-- -- public.stock_opname_detail definition

-- -- Drop table

-- -- DROP TABLE public.stock_opname_detail;

-- CREATE TABLE public.stock_opname_detail (
-- 	id bigserial NOT NULL,
-- 	stock_opname_id int8 NULL,
-- 	product_id int8 NULL,
-- 	qty numeric NULL,
-- 	hpp numeric NULL DEFAULT 0,
-- 	uom int8 NULL DEFAULT 0,
-- 	last_update_by varchar(255) NULL,
-- 	last_update timestamptz NULL,
-- 	qty_on_system numeric NULL DEFAULT 0,
-- 	CONSTRAINT stock_opname_detail_pkey PRIMARY KEY (id)
-- );


-- -- public.supplier definition

-- -- Drop table

-- -- DROP TABLE public.supplier;

-- CREATE TABLE public.supplier (
-- 	id bigserial NOT NULL,
-- 	"name" varchar(255) NULL,
-- 	code varchar(255) NULL,
-- 	address varchar(255) NULL,
-- 	city varchar(255) NULL,
-- 	pic_name varchar(255) NULL,
-- 	pic_phone varchar(255) NULL,
-- 	tax int4 NULL DEFAULT 0,
-- 	bank_id int8 NULL DEFAULT 0,
-- 	bank_acc_name varchar(255) NULL,
-- 	bank_acc_no varchar(255) NULL,
-- 	status int2 NULL,
-- 	last_update_by varchar(255) NULL,
-- 	last_update timestamptz NULL,
-- 	CONSTRAINT supplier_pkey PRIMARY KEY (id)
-- );


-- -- public.tb_sequence definition

-- -- Drop table

-- -- DROP TABLE public.tb_sequence;

-- CREATE TABLE public.tb_sequence (
-- 	id bigserial NOT NULL,
-- 	subj varchar(255) NULL,
-- 	"year" varchar(255) NULL,
-- 	"month" varchar(255) NULL,
-- 	last_seq int8 NOT NULL,
-- 	last_update_by varchar(255) NULL,
-- 	last_update timestamptz NULL
-- );


-- -- public.warehouse definition

-- -- Drop table

-- -- DROP TABLE public.warehouse;

-- CREATE TABLE public.warehouse (
-- 	id bigserial NOT NULL,
-- 	"name" varchar(255) NULL,
-- 	wh_in int2 NULL DEFAULT 1,
-- 	wh_out int2 NULL DEFAULT 1,
-- 	status int2 NULL DEFAULT 1,
-- 	code varchar(255) NULL,
-- 	last_update_by varchar(255) NULL,
-- 	last_update timestamptz NULL,
-- 	CONSTRAINT warehouse_pkey PRIMARY KEY (id)
-- );