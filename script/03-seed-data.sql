
INSERT INTO public."parameter" ("name",value,isviewable) VALUES
('tax','10',1);
INSERT INTO public."parameter" ("name",value,isviewable) VALUES
('total_record_product','100',1);


INSERT INTO public.lookup_group ("name") VALUES
	 ('Bank'),
	 ('UOM'),
	 ('Return Reason'),
	 ('Adjust Reason'),
	 ('Payment type'),
	 ('Opname type');
	 
INSERT INTO public.lookup ("name",code,lookup_group_id,status,is_viewable) VALUES
	 ('BCA','LK00001',
	 (select id from lookup_group lg  where lg."name" ='Bank')
	 ,1,1),
	 ('Mandiri','LK00002',
	 (select id from lookup_group lg  where lg."name" ='Bank')
	 ,1,1),
	 ('BRI','LK00003',
	 (select id from lookup_group lg  where lg."name" ='Bank')
	 ,1,1);
	
INSERT INTO public.lookup ("name",code,lookup_group_id,status,is_viewable) VALUES	
	 ('pc','LK00004',
	 (select id from lookup_group lg  where lg."name" ='UOM')
	 ,1,1),
	 ('box','LK00005',
	 (select id from lookup_group lg  where lg."name" ='UOM')
	 ,1,1),
	 ('renceng','LK00006',
	(select id from lookup_group lg  where lg."name" ='UOM')
	 ,1,1);
	 
INSERT INTO public.lookup ("name",code,lookup_group_id,status,is_viewable) VALUES	 
	 ('Barang Mendekati expired','LK00007',
	 (select id from lookup_group lg  where lg."name" ='Return Reason')
	 ,1,1),
	 ('Packing rusak','LK00008',
	(select id from lookup_group lg  where lg."name" ='Return Reason')
	 ,1,1);
	
INSERT INTO public.lookup ("name",code,lookup_group_id,status,is_viewable) VALUES
	 ('Barang Hilang','LK00009',
	 (select id from lookup_group lg  where lg."name" ='Adjust Reason')
	 ,1,1),
	 ('Barang cross stock','LK00010',
	(select id from lookup_group lg  where lg."name" ='Adjust Reason')
	 ,1,1),
	 ('Barang Expired','LK00011',
	(select id from lookup_group lg  where lg."name" ='Adjust Reason')
	 ,1,1);
	
INSERT INTO public.lookup ("name",code,lookup_group_id,status,is_viewable) VALUES
	 ('Cash','LK00012',
	 (select id from lookup_group lg  where lg."name" ='Payment type')
	 ,1,1),
	 ('Giro','LK00013',
	 (select id from lookup_group lg  where lg."name" ='Payment type')
	 ,1,1),
	 ('Transfer','LK00014',
	(select id from lookup_group lg  where lg."name" ='Payment type')
	 ,1,1);
	
INSERT INTO public.lookup ("name",code,lookup_group_id,status,is_viewable) VALUES
	 ('Full opname','LK00015',
	 (select id from lookup_group lg  where lg."name" ='Opname type')
	 ,1,1),
	 ('Partial opname','LK00016',
	(select id from lookup_group lg  where lg."name" ='Opname type')
	 ,1,1);
	 
INSERT INTO public.customer ("name",code,top,address1,address2,address3,address4,contact_person,phone_number,status,last_update_by,last_update) VALUES
	 ('Customer Cash','999999',0,'Cash','Cash','Cash','Cash','Cash','Cash',1,'system',CURRENT_DATE);

INSERT INTO public.sales ("name",description,status,last_update_by,last_update,code) VALUES
	 ('Sales toko','Sales toko',1,'system',CURRENT_DATE,'S00001');
	 
	
	
	

